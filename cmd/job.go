package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// MattermostMessage struct
type MattermostMessage struct {
	Channel     string                 `json:"channel"`
	Username    string                 `json:"username"`
	IconURL     string                 `json:"icon_url"`
	Text        string                 `json:"text"`
	Attachments []MattermostAttachment `json:"attachments"`
}

// MattermostAttachment struct
type MattermostAttachment struct {
	Color      string                      `json:"color"`
	Title      string                      `json:"title"`
	Text       string                      `json:"text"`
	AuthorName string                      `json:"author_name"`
	AuthorIcon string                      `json:"author_icon"`
	ThumbURL   string                      `json:"thumb_url"`
	Fields     []MattermostAttachmentField `json:"fields"`
}

// MattermostAttachmentField struct
type MattermostAttachmentField struct {
	Short bool   `json:"short"`
	Title string `json:"title"`
	Value string `json:"value"`
}

// OARJob struct
type OARJob struct {
	ID       string
	User     string
	Name     string
	WorkDir  string
	Walltime string
	Stdout   string
	Stderr   string
}

var (
	jobCmd = &cobra.Command{
		Use:   "job",
		Short: "Job messages",
		Long:  ``,
	}
)

func getOAREnv() (env OARJob) {
	env.ID = os.Getenv("OAR_JOB_ID")
	env.Name = os.Getenv("OAR_JOB_NAME")
	env.User = os.Getenv("OAR_USER")
	env.Walltime = os.Getenv("OAR_JOB_WALLTIME")
	env.WorkDir = os.Getenv("OAR_WORKDIR")
	env.Stdout = os.Getenv("OAR_STDOUT")
	env.Stderr = os.Getenv("OAR_STDERR")
	return env
}

func sendMessage(title string, color string, env OARJob) {
	var fields []MattermostAttachmentField

	fields = append(fields, MattermostAttachmentField{
		Short: true,
		Title: "Name",
		Value: env.Name,
	})
	fields = append(fields, MattermostAttachmentField{
		Short: true,
		Title: "Walltime",
		Value: env.Walltime,
	})
	fields = append(fields, MattermostAttachmentField{
		Short: true,
		Title: "Working directory",
		Value: env.WorkDir,
	})
	fields = append(fields, MattermostAttachmentField{
		Short: true,
		Title: "STDOUT file",
		Value: env.Stdout,
	})
	fields = append(fields, MattermostAttachmentField{
		Short: true,
		Title: "STDERR file",
		Value: env.Stderr,
	})

	var attachments []MattermostAttachment
	attachments = append(attachments, MattermostAttachment{
		Color:      color,
		Title:      title,
		AuthorIcon: "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
		ThumbURL:   "https://oar.imag.fr/_media/oar-logo_100px.png",
		AuthorName: fmt.Sprintf("%s OAR Webhook", viper.GetString("context")),
		Fields:     fields,
	})

	message := MattermostMessage{
		Channel:     viper.GetString("channel"),
		Username:    "OAR",
		IconURL:     "https://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
		Attachments: attachments,
	}

	jsonData, err := json.Marshal(message)
	if err != nil {
		log.Fatalf("Failed to generate json data : %s", err)
	}

	url := mattermostURL + "/hooks/" + viper.GetString("token")
	http.Post(url, "application/json", bytes.NewBuffer(jsonData))
}
