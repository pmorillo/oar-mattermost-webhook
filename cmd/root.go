package cmd

import (
	"fmt"
	"os"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile       string
	channel       string
	token         string
	mattermostURL string

	rootCmd = &cobra.Command{
		Use:   "oar-mattermost",
		Short: "A OAR Mattermost webhook",
		Long:  ``,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.oar-mattermost.yaml)")
	rootCmd.PersistentFlags().StringP("token", "t", "", "Mattermost webhook token (Required)")
	//rootCmd.MarkPersistentFlagRequired("token")
	rootCmd.PersistentFlags().StringP("channel", "c", "@"+os.Getenv("USER"), "Mattermost channel")
	rootCmd.PersistentFlags().StringVarP(&mattermostURL, "mattermost-url", "u", "https://mattermost.inria.fr", "Mattermost URL")
	rootCmd.PersistentFlags().String("context", "", "context")

	viper.BindPFlag("channel", rootCmd.PersistentFlags().Lookup("channel"))
	viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))
	viper.BindPFlag("mattermost-url", rootCmd.PersistentFlags().Lookup("mattermost-url"))
	viper.BindPFlag("context", rootCmd.PersistentFlags().Lookup("context"))

	rootCmd.AddCommand(jobCmd)
	jobCmd.AddCommand(jobStartCmd)
	jobCmd.AddCommand(jobFinishedCmd)
	jobCmd.AddCommand(jobErrorCmd)
}

func er(msg interface{}) {
	fmt.Println("Error:", msg)
	os.Exit(1)
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			er(err)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".oar-mattermost")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
