package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	jobErrorCmd = &cobra.Command{
		Use:   "error",
		Short: "Send job error message",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			env := getOAREnv()
			title := fmt.Sprintf("Job %s (%s) error", env.ID, env.User)
			sendMessage(title, "#FF8000", env)
		},
	}
)
