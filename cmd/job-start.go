package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	jobStartCmd = &cobra.Command{
		Use:   "start",
		Short: "Send job start message",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			env := getOAREnv()
			title := fmt.Sprintf("Job %s (%s) started", env.ID, env.User)
			sendMessage(title, "#36a64f", env)
		},
	}
)
