package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	jobFinishedCmd = &cobra.Command{
		Use:   "finished",
		Short: "Send job finished message",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			env := getOAREnv()
			title := fmt.Sprintf("Job %s (%s) finished", env.ID, env.User)
			sendMessage(title, "#36a64f", env)
		},
	}
)
