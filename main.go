package main

import "gitlab.inria.fr/pmorillo/oar-mattermost-webhook/cmd"

func main() {
	cmd.Execute()
}
